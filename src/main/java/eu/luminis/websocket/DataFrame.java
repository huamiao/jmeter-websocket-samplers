/*
 * Copyright 2016, 2017 Peter Doornbosch
 *
 * This file is part of JMeter-WebSocket-Samplers, a JMeter add-on for load-testing WebSocket applications.
 *
 * JMeter-WebSocket-Samplers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * JMeter-WebSocket-Samplers is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.luminis.websocket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

public abstract class DataFrame extends Frame {
	public static final int COMPRESSED_BIT = 0x40;
	public static final byte[] DECOMPRESSION_TAIL = new byte[] { 0x00, 0x00, (byte) 0xff, (byte) 0xff };

	private static final Logger log = LoggingManager.getLoggerForClass();

	protected int compBit = 0;
	protected byte[] payload;

	public DataFrame(byte[] payload) {
		this(payload, 0, true);
	}

	public DataFrame(byte[] payload, int compBit, boolean isPayloadRaw) {
		log.debug("Creating new DataFrame...");
		log.debug("Payload length is " + payload.length);
		log.debug("Compressing bit is " + compBit);
		this.payload = payload;
		if (!isPayloadRaw) {
			try {
				this.payload = this.parseInflatedPayload(payload);
			} catch (IOException e) {
				log.debug("Failed to decompress payload.", e);
			} catch (DataFormatException e) {
				log.debug("Failed to decompress payload.", e);
			}
		}

		this.compBit = compBit;
	}

	public abstract Object getData();

	@Override
	public int getRSV1() {
		return this.compBit;
	}

	private byte[] decompress(byte[] payload, byte[] dict) throws DataFormatException, IOException {
		byte[] buffer = new byte[1024];
		ByteArrayOutputStream inflatedOutputStream = new ByteArrayOutputStream();
		Inflater decompressor = WebSocketClient.threadInflater.get();

		// add [0x00,0x00,0xff,0xff] as the decompression tail because the server uses
		// compression context as default
		byte[] input = new byte[payload.length + 4];
		System.arraycopy(payload, 0, input, 0, payload.length);
		System.arraycopy(DataFrame.DECOMPRESSION_TAIL, 0, input, input.length - DataFrame.DECOMPRESSION_TAIL.length,
				DataFrame.DECOMPRESSION_TAIL.length);
		if (dict != null) {
			decompressor.setDictionary(dict);
		}
		decompressor.setInput(input);
		while (!decompressor.needsInput()) {
			int len = decompressor.inflate(buffer);
			if (len == 0) {
				break;
			}
			inflatedOutputStream.write(buffer, 0, len);
		}
		inflatedOutputStream.flush();

		return inflatedOutputStream.toByteArray();
	}

	private byte[] compress(byte[] payload, byte[] dict, boolean hasContext) throws IOException {
		byte[] buffer = new byte[1024];
		ByteArrayOutputStream deflatedOutputStream = new ByteArrayOutputStream();
		Deflater decompressor = WebSocketClient.threadDeflater.get();

		if (!hasContext) {
			decompressor.reset();
		}
		if (dict != null) {
			decompressor.setDictionary(dict);
		}
		decompressor.setInput(payload);
		if (!hasContext) {
			decompressor.finish();
		}
		while (!decompressor.needsInput()) {
			int len = decompressor.deflate(buffer, 0, buffer.length, Deflater.SYNC_FLUSH);
			if (len == 0) {
				break;
			}
			if (decompressor.needsInput() && len > 4) {
				byte[] tail = new byte[4];
				System.arraycopy(buffer, len - tail.length, tail, 0, tail.length);
				if (Arrays.equals(tail, DataFrame.DECOMPRESSION_TAIL)) {
					deflatedOutputStream.write(buffer, 0, len - tail.length);
					break;
				}
			}
			deflatedOutputStream.write(buffer, 0, len);
		}
		deflatedOutputStream.flush();

		return deflatedOutputStream.toByteArray();
	}

	protected byte[] parseInflatedPayload(byte[] payload) throws IOException, DataFormatException {
		byte[] inflatedPayload;

		log.debug("Payload is being decompressed...");

		// decompress the payload
		log.debug("Decompressing payload:" + Arrays.toString(payload));
		inflatedPayload = decompress(payload, null);

		log.debug("Decompressing is done...");
		log.debug("Decompressed payload is " + Arrays.toString(inflatedPayload));
		log.debug("Decompressed payload length is " + inflatedPayload.length);

		return inflatedPayload;
	}

	protected byte[] parseDeflatedPayload(byte[] payload) throws IOException {
		byte[] deflatedPayload;

		log.debug("Payload is being compressed...");

		// compress the payload
		log.debug("Compressing payload:" + Arrays.toString(payload));
		deflatedPayload = compress(payload, null, true);

		log.debug("Compressing is done");
		log.debug("Compressed payload is " + Arrays.toString(deflatedPayload));
		log.debug("Compressed payload length is " + deflatedPayload.length);

		return deflatedPayload;
	}

}
