/*
 * Copyright 2016, 2017 Peter Doornbosch
 *
 * This file is part of JMeter-WebSocket-Samplers, a JMeter add-on for load-testing WebSocket applications.
 *
 * JMeter-WebSocket-Samplers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * JMeter-WebSocket-Samplers is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package eu.luminis.websocket;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

public class TextFrame extends DataFrame {
	private static final Logger log = LoggingManager.getLoggerForClass();

	private byte[] deflatedPayload = null;

	public TextFrame(String text) throws IOException {
		this(text, 0);
	}

	public TextFrame(String text, int compBit) throws IOException {
		super(text.getBytes(), compBit, true);
	}

	public TextFrame(byte[] payload) throws IOException {
		this(payload, 0);
	}

	public TextFrame(byte[] payload, int compBit) throws IOException {
		super(payload, compBit, compBit == COMPRESSED_BIT ? false : true);
	}

	public String getText() {
		return new String(payload, StandardCharsets.UTF_8);
	}

	@Override
	public Object getData() {
		return this.getText();
	}

	@Override
	public boolean isText() {
		return true;
	}

	@Override
	public String toString() {
		return "Text frame with text '" + this.getText() + "'";
	}

	@Override
	protected byte[] getPayload() {
		if (compBit == DataFrame.COMPRESSED_BIT && deflatedPayload == null) {
			try {
				deflatedPayload = this.parseDeflatedPayload(payload);
				return deflatedPayload;
			} catch (IOException e) {
				log.debug("Failed to deflate payload.", e);
				return null;
			}
		} else if (compBit == DataFrame.COMPRESSED_BIT) {
			return deflatedPayload;
		}

		return payload;
	}

	@Override
	protected byte getOpCode() {
		return OPCODE_TEXT;
	}

	@Override
	public String getTypeAsString() {
		return "text";
	}

}
